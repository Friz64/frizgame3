use cranelift_codegen::{
    entity::EntityRef,
    ir::{AbiParam, InstBuilder},
    settings::Configurable,
};
use cranelift_frontend::{FunctionBuilder, FunctionBuilderContext, Variable};
use cranelift_jit::{JITBuilder, JITModule};
use cranelift_module::Module;
use eframe::egui;
use std::mem;

struct EguiApp {
    add_constant: i64,
    argument: usize,
    code_fn: Option<fn(usize) -> usize>,
    module: JITModule,
    ctx: cranelift_codegen::Context,
}

impl EguiApp {
    fn new(_cc: &eframe::CreationContext<'_>) -> EguiApp {
        let mut flag_builder = cranelift_codegen::settings::builder();
        flag_builder.set("use_colocated_libcalls", "false").unwrap();
        flag_builder.set("is_pic", "false").unwrap();
        let isa_builder = cranelift_native::builder().unwrap();
        let isa = isa_builder
            .finish(cranelift_codegen::settings::Flags::new(flag_builder))
            .unwrap();
        let jit_builder = JITBuilder::with_isa(isa, cranelift_module::default_libcall_names());
        let module = JITModule::new(jit_builder);
        let ctx = module.make_context();
        EguiApp {
            add_constant: 42,
            argument: 0,
            code_fn: None,
            module,
            ctx,
        }
    }

    fn make_function(&mut self) {
        let mut func_ctx = FunctionBuilderContext::new();

        let int = self.module.target_config().pointer_type();
        self.ctx.func.signature.params.push(AbiParam::new(int));
        self.ctx.func.signature.returns.push(AbiParam::new(int));

        let mut function_builder = FunctionBuilder::new(&mut self.ctx.func, &mut func_ctx);
        let entry_block = function_builder.create_block();
        function_builder.append_block_params_for_function_params(entry_block);
        function_builder.switch_to_block(entry_block);
        function_builder.seal_block(entry_block);

        let param_var = Variable::new(0);
        function_builder.declare_var(param_var, int);
        function_builder.def_var(param_var, function_builder.block_params(entry_block)[0]);
        let param_val = function_builder.use_var(param_var);

        let return_var = Variable::new(1);
        function_builder.declare_var(return_var, int);
        let constant = function_builder.ins().iconst(int, self.add_constant);
        let added = function_builder.ins().iadd(param_val, constant);
        function_builder.def_var(return_var, added);
        let return_val = function_builder.use_var(return_var);

        function_builder.ins().return_(&[return_val]);
        function_builder.finalize();
        // what happens to previous functions?
        let function_id = self
            .module
            .declare_anonymous_function(&self.ctx.func.signature)
            .unwrap();
        self.module
            .define_function(function_id, &mut self.ctx)
            .unwrap();

        self.module.clear_context(&mut self.ctx);
        self.module.finalize_definitions().unwrap();

        let code_ptr = self.module.get_finalized_function(function_id);
        unsafe {
            self.code_fn = Some(mem::transmute::<*const u8, fn(usize) -> usize>(code_ptr));
        }
    }
}

impl eframe::App for EguiApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label("\"hardcoded\" add constant:");
                if ui
                    .add(egui::DragValue::new(&mut self.add_constant))
                    .changed()
                {
                    self.code_fn = None;
                }
            });

            if self.code_fn.is_none() {
                self.make_function();
            }

            ui.horizontal(|ui| {
                ui.label("jitted_function(");
                ui.add(egui::DragValue::new(&mut self.argument));
                ui.label(format!(") = {}", (self.code_fn.unwrap())(self.argument)));
            });
        });
    }
}

fn main() -> eframe::Result {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "block-coding",
        native_options,
        Box::new(|cc| Ok(Box::new(EguiApp::new(cc)))),
    )
}
