#!/bin/bash
set -e # exit on errors
set -o noglob
cd $(dirname $0)

rev=$(git rev-parse --short HEAD)
dirty=$([[ -n $(git status --porcelain) ]] && echo "*" || echo)
BUILD_INFO="$rev$dirty" cargo "$@"
