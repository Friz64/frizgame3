use crate::flycam;
use bevy::{color::palettes::css, prelude::*};

pub fn plugin(app: &mut App) {
    app.add_systems(Update, editor.after(flycam::flycam));
}

#[derive(Component, Default)]
struct TrackNode {
    position: Vec3,
}

fn editor(
    mut commands: Commands,
    mut gizmos: Gizmos,
    mut ray_cast: MeshRayCast,
    mut track_nodes: Query<(Entity, &mut TrackNode)>,
    cameras: Query<&Transform, With<Camera>>,
    mouse_button_input: Res<ButtonInput<MouseButton>>,
    mut selected: Local<Option<Entity>>,
) {
    const CONTROL_POINT_RADIUS: f32 = 1.0;

    let cam_transform = cameras.single().unwrap();
    let ray = Ray3d::new(cam_transform.translation, cam_transform.forward());
    let mut hovered = None;
    if let Some((_hit_entity, hit)) = ray_cast
        .cast_ray(ray, &MeshRayCastSettings::default())
        .first()
    {
        if let Some((entity, _track_node)) = track_nodes.iter_mut().find(|(_entity, track_node)| {
            track_node.position.distance(hit.point) < CONTROL_POINT_RADIUS
        }) {
            hovered = Some(entity);
            if selected.is_none() && mouse_button_input.pressed(MouseButton::Left) {
                *selected = Some(entity);
            }

            if mouse_button_input.just_pressed(MouseButton::Right) {
                *selected = None;
                commands.entity(entity).despawn();
            }
        } else if mouse_button_input.just_pressed(MouseButton::Left) {
            commands.spawn(TrackNode {
                position: hit.point,
            });
        }

        if !mouse_button_input.pressed(MouseButton::Left) {
            *selected = None;
        }

        if let Some(selected) = *selected {
            let (_entity, mut track_node) = track_nodes.get_mut(selected).unwrap();
            track_node.position = hit.point;
        }

        gizmos.arrow(hit.point, hit.point + hit.normal, css::RED);
    }

    for (entity, track_node) in track_nodes {
        let color = if *selected == Some(entity) {
            css::DARK_RED
        } else if hovered == Some(entity) && selected.is_none() {
            css::RED
        } else {
            css::WHITE
        };

        gizmos.sphere(track_node.position, CONTROL_POINT_RADIUS, color);
    }
}
