mod editor;
mod meshing;
mod profiles;

use crate::{egui, flycam::flycam, misc::any_component_changed};
use bevy::{color::palettes::css, math::vec3, prelude::*, render::primitives::Aabb};
use profiles::UIC60;
use std::f32::consts::FRAC_PI_2;

pub fn plugin(app: &mut App) {
    app.init_resource::<TrackVariables>()
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (
                (
                    editor::editor.after(flycam),
                    compute_polylines,
                    compute_intersections.run_if(any_component_changed::<TrackPolyline>),
                    track_gizmos.after(egui),
                )
                    .chain(),
                meshing::mesh_track
                    .after(compute_intersections)
                    .run_if(any_component_changed::<TrackPolyline>),
            ),
        );
}

#[derive(Bundle)]
struct TrackBundle {
    spline: TrackSpline,
    polyline: TrackPolyline,
    intersections: TrackIntersections,
    pbr: PbrBundle,
    aabb: Aabb,
    aabb_gizmo: ShowAabbGizmo,
}

fn setup(mut commands: Commands, mut materials: ResMut<Assets<StandardMaterial>>) {
    let track_material = materials.add(StandardMaterial {
        base_color: Srgba::hex("#c9ada3").unwrap().into(),
        metallic: 0.8,
        ..Default::default()
    });

    let track = |control_points: Vec<Vec3>, gauge: f32, debug_color: Srgba| TrackBundle {
        spline: TrackSpline {
            control_points,
            gauge,
        },
        polyline: TrackPolyline {
            samples: Vec::new(),
            debug_color,
        },
        intersections: TrackIntersections::default(),
        pbr: PbrBundle {
            material: track_material.clone(),
            ..Default::default()
        },
        aabb: Aabb::default(),
        aabb_gizmo: ShowAabbGizmo::default(),
    };

    commands.spawn(track(
        vec![
            vec3(23.424301, -1.0646845e-6, 17.86244),
            vec3(4.686802, 7.992143e-6, -6.085908),
            vec3(-25.292645, -1.529892e-5, 0.6733026),
            vec3(-16.183674, -1.9654524e-6, 32.974815),
            vec3(16.159893, 0.0, 33.737816),
            vec3(36.278004, -6.211898e-5, 18.183634),
            vec3(43.078514, -3.0033856e-5, -8.115489),
        ],
        1.435,
        Srgba::GREEN,
    ));

    commands.spawn(track(
        vec![
            vec3(-32.2789, -4.7623183e-5, 30.98448),
            vec3(-41.24146, 8.298332e-7, -13.92229),
            vec3(-7.320671, 4.058408e-5, -40.887947),
            vec3(15.338821, -5.877995e-6, -29.383593),
            vec3(30.230043, 3.0553645e-5, -0.6051282),
            vec3(4.367199, 1.4548841e-5, 11.910929),
            vec3(-21.334797, -2.9655776e-5, -14.458615),
        ],
        1.0,
        Srgba::RED,
    ));
}

#[derive(Resource, Clone, Copy)]
pub struct TrackVariables {
    pub sample_resolution: usize,
    pub significant_track_angle: f32,
}

impl Default for TrackVariables {
    fn default() -> Self {
        Self {
            sample_resolution: 20,
            significant_track_angle: 1.5,
        }
    }
}

#[derive(Component)]
struct TrackSpline {
    control_points: Vec<Vec3>,
    gauge: f32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Rail {
    Left,
    Right,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum RailSide {
    Inner,
    Outer,
}

#[derive(Clone, Copy)]
struct CurveSample {
    i: usize,
    left_inner_pos: Vec3,
    left_outer_pos: Vec3,
    right_inner_pos: Vec3,
    right_outer_pos: Vec3,
    vel: Vec3,
}

impl CurveSample {
    // todo: support rails that don't point straight up?
    fn new(gauge: f32, profile_width: f32, i: usize, pos: Vec3, vel: Vec3) -> CurveSample {
        let dir_right = Mat2::from_angle(FRAC_PI_2) * vel.xz().normalize();
        let dir_right = Vec3::new(dir_right.x, 0.0, dir_right.y);
        let gauge_offset = dir_right * gauge * 0.5;

        let left_inner_pos = pos + gauge_offset;
        let right_inner_pos = pos - gauge_offset;
        CurveSample {
            i,
            left_inner_pos,
            left_outer_pos: left_inner_pos + dir_right * profile_width,
            right_inner_pos,
            right_outer_pos: right_inner_pos - dir_right * profile_width,
            vel,
        }
    }

    fn pos(&self, rail: Rail, side: RailSide) -> Vec3 {
        match (rail, side) {
            (Rail::Left, RailSide::Inner) => self.left_inner_pos,
            (Rail::Left, RailSide::Outer) => self.left_outer_pos,
            (Rail::Right, RailSide::Inner) => self.right_inner_pos,
            (Rail::Right, RailSide::Outer) => self.right_outer_pos,
        }
    }
}

#[derive(Component)]
struct TrackPolyline {
    samples: Vec<CurveSample>,
    debug_color: Srgba,
}

fn compute_polylines(
    vars: Res<TrackVariables>,
    mut splines: Query<(Ref<TrackSpline>, &mut TrackPolyline)>,
) {
    for (spline, mut polyline) in &mut splines {
        if !spline.is_changed() && !vars.is_changed() {
            continue;
        }

        let curve = CubicCardinalSpline::new_catmull_rom(spline.control_points.clone()).to_curve();
        let subdivisions = curve.segments.len() * vars.sample_resolution;
        let step = curve.segments.len() as f32 / subdivisions as f32;

        // This is copied together from Bevy source code because their methods aren't public
        let samples = (0..=subdivisions).map(move |i| {
            let t = i as f32 * step;
            let (segment, t) = if curve.segments.len() == 1 {
                (&curve.segments[0], t)
            } else {
                let i = (t.floor() as usize).clamp(0, curve.segments.len() - 1);
                (&curve.segments[i], t - i as f32)
            };

            CurveSample::new(
                spline.gauge,
                UIC60.head_width,
                i,
                segment.position(t),
                segment.velocity(t),
            )
        });

        let mut previous_vel = Vec3::ZERO;
        polyline.samples.clear();
        for sample in samples {
            if [0, subdivisions].contains(&sample.i)
                || sample.vel.angle_between(previous_vel)
                    > f32::to_radians(vars.significant_track_angle)
            {
                previous_vel = sample.vel;
                polyline.samples.push(sample);
            }
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum IntersectionOrientation {
    FromLeft,
    FromRight,
}

#[derive(Debug)]
struct TrackIntersection {
    rail: Rail,
    other_rail: Rail,
    direction: Vec2,
    other_direction: Vec2,
    segment: usize,
    inner_t: f32, // 0..=1 within the segment
    outer_t: f32, // 0..=1 within the segment
}

impl TrackIntersection {
    fn point(&self, polyline: &TrackPolyline, side: RailSide) -> Vec3 {
        let segment = polyline.samples.windows(2).nth(self.segment).unwrap();
        Vec3::lerp(
            segment[0].pos(self.rail, side),
            segment[1].pos(self.rail, side),
            match side {
                RailSide::Inner => self.inner_t,
                RailSide::Outer => self.outer_t,
            },
        )
    }

    fn orientation(&self) -> IntersectionOrientation {
        if Mat2::from_cols(self.direction, self.other_direction).determinant() > 0.0 {
            IntersectionOrientation::FromRight
        } else {
            IntersectionOrientation::FromLeft
        }
    }
}

#[derive(Component, Default)]
struct TrackIntersections {
    list: Vec<TrackIntersection>,
}

impl TrackIntersections {
    fn collect_into(
        intersections_query: &mut Query<&mut TrackIntersections>,
        track1: Entity,
        segment1_idx: usize,
        segment1: &[CurveSample; 2],
        track2: Entity,
        segment2_idx: usize,
        segment2: &[CurveSample; 2],
    ) {
        fn segment_segment_intersection(
            segment1_begin: Vec2,
            segment1_end: Vec2,
            segment2_begin: Vec2,
            segment2_end: Vec2,
        ) -> (f32, f32) {
            // segment-segment intersection algorithm
            // https://stackoverflow.com/a/1968345
            let (p1, p2) = (segment1_begin, segment2_begin);
            let (s1, s2) = (segment1_end - p1, segment2_end - p2);
            let segment1_t =
                (s2.x * (p1.y - p2.y) - s2.y * (p1.x - p2.x)) / (-s2.x * s1.y + s1.x * s2.y);
            let segment2_t =
                (-s1.y * (p1.x - p2.x) + s1.x * (p1.y - p2.y)) / (-s2.x * s1.y + s1.x * s2.y);
            (segment1_t, segment2_t)
        }

        let segment1l = (
            segment1.map(|r| r.pos(Rail::Left, RailSide::Inner).xz()),
            segment1.map(|r| r.pos(Rail::Left, RailSide::Outer).xz()),
        );
        let segment1r = (
            segment1.map(|r| r.pos(Rail::Right, RailSide::Inner).xz()),
            segment1.map(|r| r.pos(Rail::Right, RailSide::Outer).xz()),
        );
        let segment2l = (
            segment2.map(|r| r.pos(Rail::Left, RailSide::Inner).xz()),
            segment2.map(|r| r.pos(Rail::Left, RailSide::Outer).xz()),
        );
        let segment2r = (
            segment2.map(|r| r.pos(Rail::Right, RailSide::Inner).xz()),
            segment2.map(|r| r.pos(Rail::Right, RailSide::Outer).xz()),
        );

        for ((rail1, (segment1i, segment1o)), (rail2, (segment2i, segment2o))) in [
            ((Rail::Left, segment1l), (Rail::Left, segment2l)),
            ((Rail::Right, segment1r), (Rail::Right, segment2r)),
            ((Rail::Left, segment1l), (Rail::Right, segment2r)),
            ((Rail::Right, segment1r), (Rail::Left, segment2l)),
        ] {
            let (s1i_t, s2i_t) = segment_segment_intersection(
                segment1i[0],
                segment1i[1],
                segment2i[0],
                segment2i[1],
            );

            if (0.0..=1.0).contains(&s1i_t) && (0.0..=1.0).contains(&s2i_t) {
                let (s1o_t, s2o_t) = segment_segment_intersection(
                    segment1o[0],
                    segment1o[1],
                    segment2o[0],
                    segment2o[1],
                );

                let s1dir = segment1i[1] - segment1i[0];
                let s2dir = segment2i[1] - segment2i[0];

                (intersections_query.get_mut(track1).unwrap())
                    .list
                    .push(TrackIntersection {
                        rail: rail1,
                        other_rail: rail2,
                        direction: s1dir,
                        other_direction: s2dir,
                        segment: segment1_idx,
                        inner_t: s1i_t,
                        outer_t: s1o_t,
                    });

                (intersections_query.get_mut(track2).unwrap())
                    .list
                    .push(TrackIntersection {
                        rail: rail2,
                        other_rail: rail1,
                        direction: s2dir,
                        other_direction: s1dir,
                        segment: segment2_idx,
                        inner_t: s2i_t,
                        outer_t: s2o_t,
                    });
            }
        }
    }
}

fn compute_intersections(
    polylines: Query<(Entity, &TrackPolyline)>,
    mut intersections_query: Query<&mut TrackIntersections>,
) {
    for mut intersection in &mut intersections_query {
        intersection.list.clear();
    }

    // intersections with other tracks
    // TODO: use AABBs to prevent exponential algorithm?
    for [(track1, polyline1), (track2, polyline2)] in polylines.iter_combinations() {
        for (segment1_idx, segment1) in polyline1.samples.windows(2).enumerate() {
            let segment1 = segment1.try_into().unwrap();
            for (segment2_idx, segment2) in polyline2.samples.windows(2).enumerate() {
                let segment2 = segment2.try_into().unwrap();
                TrackIntersections::collect_into(
                    &mut intersections_query,
                    track1,
                    segment1_idx,
                    segment1,
                    track2,
                    segment2_idx,
                    segment2,
                );
            }
        }
    }

    // self intersections
    for (track, polyline) in polylines.iter() {
        for (segment1_idx, segment1) in polyline.samples.windows(2).enumerate() {
            let segment1 = segment1.try_into().unwrap();
            for (segment2_idx, segment2) in
                polyline.samples.windows(2).enumerate().skip(segment1_idx)
            {
                if segment1_idx.abs_diff(segment2_idx) >= 2 {
                    let segment2 = segment2.try_into().unwrap();
                    TrackIntersections::collect_into(
                        &mut intersections_query,
                        track,
                        segment2_idx,
                        segment2,
                        track,
                        segment1_idx,
                        segment1,
                    );
                }
            }
        }
    }

    for mut intersection in &mut intersections_query {
        (intersection.list).sort_by(|a, b| a.inner_t.partial_cmp(&b.inner_t).unwrap());
    }
}

fn track_gizmos(
    mut gizmos: Gizmos,
    polylines: Query<(&TrackPolyline, &TrackIntersections, &Transform)>,
) {
    for (polyline, intersections, aabb) in &polylines {
        for intersection in &intersections.list {
            let inner_point = intersection.point(polyline, RailSide::Inner);
            let outer_point = intersection.point(polyline, RailSide::Outer);
            gizmos.line(
                inner_point + Vec3::Y * 0.2,
                outer_point + Vec3::Y * 0.2,
                polyline.debug_color,
            );
        }

        gizmos.linestrip(
            polyline
                .samples
                .iter()
                .map(|sample| sample.pos(Rail::Left, RailSide::Inner) + Vec3::Y * 0.2),
            css::GREEN,
        );

        gizmos.linestrip(
            polyline
                .samples
                .iter()
                .map(|sample| sample.pos(Rail::Right, RailSide::Inner) + Vec3::Y * 0.2),
            css::DARK_RED,
        );

        // for rail in [Rail::Left, Rail::Right] {
        //     for sample in polyline.samples.iter() {
        //         let pos = sample.pos(rail, RailSide::Inner);
        //         gizmos.line(pos, pos + Vec3::Y, Color::WHITE);
        //     }
        // }

        gizmos.sphere(aabb.translation, Quat::IDENTITY, 5.0, css::FUCHSIA);
    }
}
