use bevy::prelude::*;
use bevy_egui::egui;

pub fn _any_component_changed<T: Component>(query: Query<(), Changed<T>>) -> bool {
    !query.is_empty()
}

pub fn egui_change_detection<T: Resource>(
    value: &mut ResMut<T>,
    f: impl FnOnce(&mut T) -> egui::Response,
) {
    if f(value.bypass_change_detection()).changed() {
        value.set_changed();
    }
}
