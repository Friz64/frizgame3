use std::{
    f32::consts::{FRAC_PI_2, PI},
    iter,
};

use super::{
    profiles::UIC60, IntersectionOrientation, Rail, RailSide, TrackIntersections, TrackPolyline,
};
use bevy::{
    prelude::*,
    render::{
        mesh::{Indices, PrimitiveTopology, VertexAttributeValues},
        primitives::Aabb,
        render_asset::RenderAssetUsages,
    },
};

pub struct MeshBuilder {
    triangulation: Vec<usize>,
    // temporary buffers:
    positions: Vec<Vec3>,
    uv_0s: Vec<Vec2>,
    indices: Vec<u16>,
}

impl Default for MeshBuilder {
    fn default() -> Self {
        Self {
            triangulation: UIC60.triangulate(),
            positions: Vec::new(),
            uv_0s: Vec::new(),
            indices: Vec::new(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ProfileContext {
    RailStart,
    Normal,
    OtherConnectStart,
    OtherConnectEnd,
    RailEnd,
}

pub struct MeshingInstruction {
    pos: Vec3,
    dir: Vec2,
    width_scale: f32,
    rail: Rail,
    context: ProfileContext,
}

impl MeshBuilder {
    fn place_profile(&mut self, instruction: &MeshingInstruction) {
        let profile = UIC60.vertices.len();
        let start_i = self.positions.len();

        let transform = Transform {
            translation: instruction.pos,
            rotation: Quat::from_axis_angle(
                // make it face the curve
                Vec3::new(instruction.dir.x, 0.0, instruction.dir.y)
                    .normalize()
                    .cross(-Vec3::Y),
                FRAC_PI_2,
            ) * Quat::from_axis_angle(
                // always face upwards
                Vec3::Y,
                -f32::atan2(instruction.dir.y, instruction.dir.x) + FRAC_PI_2,
            ),
            scale: Vec3::new(instruction.width_scale, 1.0, 1.0),
        };

        let mut append_vertices = || {
            let instance_i = self.positions.len();
            match instruction.rail {
                Rail::Left => {
                    self.positions.extend(UIC60.vertices.iter().map(|v| {
                        let point = Vec3::new(v.x, 0.0, v.y);
                        transform.transform_point(point)
                    }));
                }
                Rail::Right => {
                    self.positions.extend(UIC60.vertices.iter().rev().map(|v| {
                        let point = Vec3::new(-v.x, 0.0, v.y);
                        transform.transform_point(point)
                    }));
                }
            }

            self.uv_0s.extend(iter::repeat(Vec2::ZERO).take(profile));
            instance_i
        };

        if matches!(
            instruction.context,
            ProfileContext::RailStart | ProfileContext::RailEnd
        ) {
            let filler_i = append_vertices();
            match instruction.context {
                ProfileContext::RailStart => self
                    .indices
                    .extend(self.triangulation.iter().map(|i| (filler_i + i) as u16)),
                ProfileContext::RailEnd => self
                    .indices
                    .extend((self.triangulation.iter().rev()).map(|i| (filler_i + i) as u16)),
                _ => unreachable!(),
            }
        }

        let sample_i = append_vertices();

        // define quads to connect to previous profile
        if matches!(
            instruction.context,
            ProfileContext::Normal | ProfileContext::OtherConnectStart | ProfileContext::RailEnd
        ) {
            let sample_ia = start_i as u16 - profile as u16;
            let sample_ib = sample_i as u16;
            for profile_ia in 0..profile as u16 {
                let profile_ib = (profile_ia + 1) % profile as u16;
                self.indices.extend([
                    // triangle 1
                    sample_ia + profile_ia,
                    sample_ia + profile_ib,
                    sample_ib + profile_ia,
                    // triangle 2
                    sample_ia + profile_ib,
                    sample_ib + profile_ib,
                    sample_ib + profile_ia,
                ]);
            }
        }
    }

    fn finish(&mut self, mesh: &mut Mesh) {
        if let VertexAttributeValues::Float32x3(positions) =
            mesh.attribute_mut(Mesh::ATTRIBUTE_POSITION).unwrap()
        {
            positions.clear();
            positions.extend(self.positions.drain(..).map(<[f32; 3]>::from));
        } else {
            unreachable!()
        }

        if let VertexAttributeValues::Float32x2(uv_0s) =
            mesh.attribute_mut(Mesh::ATTRIBUTE_UV_0).unwrap()
        {
            uv_0s.clear();
            uv_0s.extend(self.uv_0s.drain(..).map(<[f32; 2]>::from));
        } else {
            unreachable!()
        }

        if let Indices::U16(indices) = mesh.indices_mut().unwrap() {
            indices.clear();
            indices.append(&mut self.indices);
        } else {
            unreachable!()
        }
    }
}

pub(super) fn mesh_track(
    mut builder: Local<MeshBuilder>,
    mut instructions: Local<Vec<MeshingInstruction>>,
    mut tracks: Query<(
        &TrackPolyline,
        &TrackIntersections,
        &mut Handle<Mesh>,
        &mut Transform,
        &mut Aabb,
    )>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut gizmos: Gizmos,
) {
    instructions.clear();
    for (polyline, intersections, mut mesh_handle, mut transform, mut aabb) in &mut tracks {
        for rail in [Rail::Left, Rail::Right] {
            let mut inside_other_track = false;
            let sample_count = polyline.samples.len();
            for (sample_number, sample) in polyline.samples.iter().enumerate() {
                instructions.push(MeshingInstruction {
                    pos: sample.pos(rail, RailSide::Inner),
                    dir: sample.vel.xz(),
                    width_scale: 1.0,
                    rail,
                    context: match sample_number {
                        0 => ProfileContext::RailStart,
                        n if n == sample_count - 1 => ProfileContext::RailEnd,
                        _ => ProfileContext::Normal,
                    },
                });

                for intersection in intersections.list.iter().filter(|intersection| {
                    intersection.rail == rail && intersection.segment == sample_number
                }) {
                    let inner_pos = intersection.point(polyline, RailSide::Inner);
                    let outer_pos = intersection.point(polyline, RailSide::Outer);
                    let inner_outer_dir = inner_pos.xz() - outer_pos.xz();
                    let orientation = intersection.orientation();

                    let angle_to_inner_outer =
                        intersection.direction.angle_between(inner_outer_dir);
                    let on_width_scale = 1.0 / angle_to_inner_outer.sin().abs();
                    let off_width_scale = 1.0 / angle_to_inner_outer.cos().abs();

                    let rail_rot = Mat2::from_angle(match rail {
                        Rail::Left => FRAC_PI_2,
                        Rail::Right => -FRAC_PI_2,
                    });

                    let gap_offset =
                        Vec3::new(intersection.direction.x, 0.0, intersection.direction.y)
                            .normalize()
                            * f32::max(on_width_scale, off_width_scale)
                            * 0.1;
                    if inside_other_track {
                        instructions.push(MeshingInstruction {
                            pos: inner_pos - gap_offset,
                            dir: Mat2::from_angle(PI) * inner_outer_dir,
                            width_scale: off_width_scale,
                            rail,
                            context: ProfileContext::OtherConnectStart,
                        });

                        instructions.push(MeshingInstruction {
                            pos: inner_pos,
                            dir: rail_rot * inner_outer_dir,
                            width_scale: on_width_scale,
                            rail,
                            context: ProfileContext::OtherConnectEnd,
                        });
                    } else {
                        instructions.push(MeshingInstruction {
                            pos: inner_pos,
                            dir: rail_rot * inner_outer_dir,
                            width_scale: on_width_scale,
                            rail,
                            context: ProfileContext::OtherConnectStart,
                        });

                        instructions.push(MeshingInstruction {
                            pos: inner_pos + gap_offset,
                            dir: inner_outer_dir,
                            width_scale: off_width_scale,
                            rail,
                            context: ProfileContext::OtherConnectEnd,
                        });
                    }

                    inside_other_track = match (orientation, intersection.other_rail) {
                        (IntersectionOrientation::FromLeft, Rail::Left)
                        | (IntersectionOrientation::FromRight, Rail::Right) => false,
                        (IntersectionOrientation::FromLeft, Rail::Right)
                        | (IntersectionOrientation::FromRight, Rail::Left) => true,
                    };
                }
            }
        }

        for instruction in &instructions {
            let is_other_connect = |instruction: &&MeshingInstruction| {
                matches!(
                    instruction.context,
                    ProfileContext::OtherConnectStart | ProfileContext::OtherConnectEnd
                )
            };

            let mut allowed = true;
            if instruction.context == ProfileContext::Normal {
                // todo: this probably stinks
                for intersection in instructions.iter().filter(|instruction| {
                    matches!(
                        instruction.context,
                        ProfileContext::OtherConnectStart | ProfileContext::OtherConnectEnd
                    )
                }) {
                    if instruction.pos.distance(intersection.pos)
                        < UIC60.foot_width * intersection.width_scale
                    {
                        gizmos.line(
                            instruction.pos + Vec3::Y * 0.3,
                            intersection.pos + Vec3::Y * 0.3,
                            bevy::color::palettes::css::DARK_CYAN,
                        );
                        allowed = false;
                        break;
                    }
                }
            }

            if allowed {
                gizmos.line(
                    instruction.pos,
                    instruction.pos + Vec3::Y,
                    if is_other_connect(&instruction) {
                        bevy::color::palettes::css::LIGHT_SALMON
                    } else {
                        bevy::color::palettes::css::WHITE
                    },
                );
                builder.place_profile(instruction);
            } else {
                gizmos.line(instruction.pos, instruction.pos + Vec3::Y, Color::BLACK);
            }
        }

        // TODO: I haven't measured if this is a bottleneck but theoretically --
        // this could be sped up by computing the bounding box from the samples
        // and then growing it by a margin based on the profile?
        *aabb = Aabb::enclosing(&builder.positions).unwrap();

        // store position in transform instead of vertex data
        let min = aabb.min();
        aabb.center -= min;
        let min = Vec3::from(min);
        transform.translation = min;
        for v in builder.positions.iter_mut() {
            *v -= min;
        }

        if meshes.get(mesh_handle.id()).is_none() {
            *mesh_handle = meshes.add(
                Mesh::new(
                    PrimitiveTopology::TriangleList,
                    RenderAssetUsages::MAIN_WORLD | RenderAssetUsages::RENDER_WORLD,
                )
                .with_inserted_attribute(Mesh::ATTRIBUTE_POSITION, Vec::<[f32; 3]>::new())
                .with_inserted_attribute(Mesh::ATTRIBUTE_UV_0, Vec::<[f32; 2]>::new())
                .with_inserted_indices(Indices::U16(Vec::new())),
            );
        }

        let mesh = meshes.get_mut(mesh_handle.id()).unwrap();
        builder.finish(mesh);
        mesh.compute_normals();
    }
}
