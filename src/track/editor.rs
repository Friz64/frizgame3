use super::TrackSpline;
use bevy::{color::palettes::css, prelude::*};
use bevy_mod_raycast::immediate::{Raycast, RaycastSettings};

pub(super) struct SelectedControlPoint {
    entity: Entity,
    index: usize,
}

pub(super) fn editor(
    mut gizmos: Gizmos,
    cameras: Query<&Transform, With<Camera>>,
    mut raycast: Raycast,
    mut splines: Query<(Entity, &mut TrackSpline)>,
    mouse_button_input: Res<ButtonInput<MouseButton>>,
    mut selected: Local<Option<SelectedControlPoint>>,
) {
    // if mouse_button_input.just_pressed(MouseButton::Middle) {
    //     for (_, t) in &tracks {
    //         println!("control_points: vec![");
    //         for c in &t.control_points {
    //             println!("{},", format!("{:?}", c).to_lowercase());
    //         }
    //         println!("],");
    //     }
    // }

    let cam_transform = cameras.single();

    let raycast_result = raycast
        .cast_ray(
            Ray3d::new(cam_transform.translation, cam_transform.forward().into()),
            &RaycastSettings::default(),
        )
        .first();

    if !mouse_button_input.pressed(MouseButton::Left) {
        *selected = None;
    }

    let selection_distance = 1.0;

    if let Some((_entity, raycast_data)) = raycast_result {
        gizmos.arrow(
            raycast_data.position(),
            raycast_data.position() + raycast_data.normal(),
            css::RED,
        );

        for (entity, spline) in &splines {
            for (i, control_point) in spline.control_points.iter().enumerate() {
                if selected.is_none()
                    && control_point.distance(raycast_data.position()) < selection_distance
                    && mouse_button_input.pressed(MouseButton::Left)
                {
                    *selected = Some(SelectedControlPoint { entity, index: i });
                }
            }
        }

        if let Some(SelectedControlPoint {
            entity: track,
            index,
        }) = *selected
        {
            if let Ok((_entity, mut spline)) = splines.get_mut(track) {
                spline.control_points[index] = raycast_data.position();
            }
        }
    }

    for (entity, spline) in &splines {
        for (i, control_point) in spline.control_points.iter().enumerate() {
            let mut color = css::WHITE;
            if let Some(selected) = &*selected {
                if selected.entity == entity && selected.index == i {
                    color = css::DARK_RED;
                }
            } else if let Some((_entity, raycast_data)) = raycast_result {
                if control_point.distance(raycast_data.position()) < selection_distance {
                    color = css::RED;
                }
            }

            gizmos.circle(*control_point, Dir3::Y, selection_distance, color);
        }
    }
}
