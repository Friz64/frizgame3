use bevy::{input::mouse::MouseMotion, prelude::*, window::CursorGrabMode};
use bevy_egui::{
    egui::{self, emath},
    EguiContexts,
};

pub fn plugin(app: &mut App) {
    app.add_systems(Update, (crosshair, flycam));
}

fn crosshair(mut contexts: EguiContexts) {
    egui::Area::new("crosshair".into())
        .anchor(egui::Align2::CENTER_CENTER, emath::Vec2::ZERO)
        .show(contexts.ctx_mut(), |ui| {
            let size = emath::Vec2::splat(8.0);
            let (response, painter) = ui.allocate_painter(size, egui::Sense::hover());
            let rect = response.rect;
            let c = rect.center();
            let r = rect.width() / 2.0 - 1.0;
            let color = egui::Color32::WHITE;
            let stroke = egui::Stroke::new(1.0, color);
            painter.circle_stroke(c, r, stroke);
        });
}

pub fn flycam(
    time: Res<Time>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut mouse_motion_events: EventReader<MouseMotion>,
    mut cameras: Query<&mut Transform, With<Camera>>,
    mut windows: Query<&mut Window>,
    mut grabbed: Local<bool>,
) {
    let mut cam_transform = cameras.single_mut().unwrap();
    let mut window = windows.single_mut().unwrap();

    let forward = cam_transform.forward().as_vec3();
    let mut translation = Vec3::ZERO;

    if keyboard_input.pressed(KeyCode::KeyW) {
        translation += forward;
    }

    if keyboard_input.pressed(KeyCode::KeyS) {
        translation += -forward;
    }

    if keyboard_input.pressed(KeyCode::KeyA) {
        translation += -forward.cross(Vec3::Y);
    }

    if keyboard_input.pressed(KeyCode::KeyD) {
        translation += forward.cross(Vec3::Y);
    }

    cam_transform.translation += translation.normalize_or_zero()
        * time.delta_secs()
        * if keyboard_input.pressed(KeyCode::ControlLeft) {
            1.0
        } else if keyboard_input.pressed(KeyCode::ShiftLeft) {
            100.0
        } else {
            20.0
        };

    let keyboard_rotation_multiplier = time.delta_secs() * 2.0;
    if keyboard_input.pressed(KeyCode::KeyO) {
        cam_transform.rotate_local_axis(Dir3::X, keyboard_rotation_multiplier);
    }

    if keyboard_input.pressed(KeyCode::KeyL) {
        cam_transform.rotate_local_axis(Dir3::X, -keyboard_rotation_multiplier);
    }

    if keyboard_input.pressed(KeyCode::KeyK) {
        cam_transform.rotate_y(keyboard_rotation_multiplier);
    }

    if keyboard_input.pressed(KeyCode::Semicolon) {
        cam_transform.rotate_y(-keyboard_rotation_multiplier);
    }

    if keyboard_input.just_pressed(KeyCode::KeyG) {
        *grabbed ^= true;
        window.cursor_options.visible = !*grabbed;
        window.cursor_options.grab_mode = if *grabbed {
            CursorGrabMode::Locked
        } else {
            CursorGrabMode::None
        };
    }

    if *grabbed {
        let cursor_delta: Vec2 = mouse_motion_events
            .read()
            .map(|mouse_motion| mouse_motion.delta)
            .fold(Vec2::ZERO, |acc, x| acc + x)
            * 0.0025;
        cam_transform.rotate_y(-cursor_delta.x);
        cam_transform.rotate_local_x(-cursor_delta.y);
    }
}
