#![allow(clippy::excessive_precision, clippy::too_many_arguments)]
mod flycam;
mod misc;
mod track;

use bevy::{core_pipeline::Skybox, pbr::wireframe::WireframeConfig, prelude::*};
use bevy_egui::{egui, EguiContexts, EguiPlugin};
use misc::egui_change_detection;
use std::f32::consts::{FRAC_PI_2, PI};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: format!(
                    "{} ({})",
                    env!("CARGO_PKG_NAME"),
                    option_env!("BUILD_INFO").unwrap_or("unknown build")
                ),
                ..Default::default()
            }),
            ..Default::default()
        }))
        .add_plugins(MeshPickingPlugin)
        // .add_plugins(bevy::pbr::wireframe::WireframePlugin)
        .insert_resource(WireframeConfig {
            global: true,
            ..Default::default()
        })
        .add_plugins(EguiPlugin)
        .add_plugins(flycam::plugin)
        .add_plugins(track::plugin)
        .add_systems(Startup, setup)
        .init_resource::<SillyValue>()
        .add_systems(Update, egui)
        .add_systems(
            PostUpdate,
            close_on_esc.before(bevy::window::exit_on_all_closed),
        )
        .run();
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    commands.spawn((
        Mesh3d(meshes.add(Circle::new(50.0))),
        MeshMaterial3d(materials.add(Color::WHITE)),
        Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2)),
    ));

    // directional 'sun' light
    commands.spawn((
        DirectionalLight {
            illuminance: light_consts::lux::OVERCAST_DAY,
            shadows_enabled: true,
            ..default()
        },
        Transform {
            translation: Vec3::new(0.0, 2.0, 0.0),
            rotation: Quat::from_rotation_x(-PI / 4.),
            ..default()
        },
    ));

    let skybox_handle = asset_server.load("wgpu_skybox.ktx2");
    commands.spawn((
        Camera3d::default(),
        Camera {
            clear_color: ClearColorConfig::None,
            ..default()
        },
        Transform::from_xyz(-30.0, 10.0, 30.0).looking_at(Vec3::ZERO, Vec3::Y),
        Skybox {
            image: skybox_handle.clone(),
            brightness: 1000.0,
            rotation: Quat::IDENTITY,
        },
    ));
}

#[derive(Resource, Default)]
struct SillyValue {
    the_value: u32,
}

fn egui(mut contexts: EguiContexts, mut silly_value: ResMut<SillyValue>) {
    egui::Window::new("debug").show(contexts.ctx_mut(), |ui| {
        egui_change_detection(&mut silly_value, |silly_value| {
            ui.label("silly value");
            ui.add(egui::Slider::new(&mut silly_value.the_value, 1..=100))
        });
    });
}

pub fn close_on_esc(
    mut commands: Commands,
    focused_windows: Query<(Entity, &Window)>,
    input: Res<ButtonInput<KeyCode>>,
) {
    for (window, focus) in focused_windows.iter() {
        if !focus.focused {
            continue;
        }

        if input.just_pressed(KeyCode::Escape) {
            commands.entity(window).despawn();
        }
    }
}
