#![allow(clippy::excessive_precision, clippy::too_many_arguments)]
mod flycam;
mod misc;
mod track;

use bevy::{core_pipeline::Skybox, pbr::wireframe::WireframeConfig, prelude::*};
use bevy_egui::{egui, EguiContexts, EguiPlugin};
use misc::egui_change_detection;
use std::f32::consts::{FRAC_PI_2, PI};
use track::TrackVariables;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: format!(
                    "{} ({})",
                    env!("CARGO_PKG_NAME"),
                    option_env!("BUILD_INFO").unwrap_or("unknown build")
                ),
                ..Default::default()
            }),
            ..Default::default()
        }))
        // .add_plugins(bevy::pbr::wireframe::WireframePlugin)
        .insert_resource(WireframeConfig {
            global: true,
            ..Default::default()
        })
        .add_plugins(EguiPlugin)
        .add_plugins((track::plugin, flycam::plugin))
        .insert_resource(ClearColor(Color::srgb_u8(30, 30, 30)))
        .add_systems(Startup, setup)
        .add_systems(Update, egui)
        .add_systems(
            PostUpdate,
            close_on_esc.before(bevy::window::exit_on_all_closed),
        )
        .run();
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    commands.spawn(PbrBundle {
        mesh: meshes.add(Circle::new(50.0)),
        material: materials.add(Color::WHITE),
        transform: Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2)),
        ..default()
    });

    // directional 'sun' light
    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            illuminance: light_consts::lux::OVERCAST_DAY,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform {
            translation: Vec3::new(0.0, 2.0, 0.0),
            rotation: Quat::from_rotation_x(-PI / 4.),
            ..default()
        },
        ..default()
    });

    let skybox_handle = asset_server.load("wgpu_skybox.ktx2");
    commands.spawn((
        Camera3dBundle {
            transform: Transform::from_xyz(-3.5, 10.5, 9.0).looking_at(Vec3::ZERO, Vec3::Y),
            camera: Camera {
                clear_color: ClearColorConfig::None,
                ..default()
            },
            ..default()
        },
        Skybox {
            image: skybox_handle.clone(),
            brightness: 1000.0,
        },
    ));
}

fn egui(mut contexts: EguiContexts, mut vars: ResMut<TrackVariables>) {
    egui::Window::new("debug").show(contexts.ctx_mut(), |ui| {
        egui_change_detection(&mut vars, |vars| {
            ui.label("sample_resolution");
            ui.add(egui::Slider::new(&mut vars.sample_resolution, 1..=100))
        });

        ui.separator();
        egui_change_detection(&mut vars, |vars| {
            ui.label("significant_track_angle");
            ui.add(egui::Slider::new(
                &mut vars.significant_track_angle,
                0.0..=20.0,
            ))
        });
    });
}

pub fn close_on_esc(
    mut commands: Commands,
    focused_windows: Query<(Entity, &Window)>,
    input: Res<ButtonInput<KeyCode>>,
) {
    for (window, focus) in focused_windows.iter() {
        if !focus.focused {
            continue;
        }

        if input.just_pressed(KeyCode::Escape) {
            commands.entity(window).despawn();
        }
    }
}
