<img src="logo.png" height=200>

# frizgame3

Download the latest build for Linux [here](https://gitlab.com/Friz64/frizgame3/-/jobs/artifacts/main/download?job=cargo).

Download the [GPN 22](https://entropia.de/GPN22) build for Linux [here](https://gitlab.com/Friz64/frizgame3/-/jobs/artifacts/gpn2024/download?job=cargo).

## License

This project is free and open source. All code in this repository is dual-licensed under either:

- MIT License ([LICENSE-MIT](/LICENSE-MIT) or <http://opensource.org/licenses/MIT>)
- Apache License, Version 2.0 ([LICENSE-APACHE](/LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)

at your option.
